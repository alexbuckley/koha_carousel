#/bin/bash
if [ ! -d /usr/share/newbooks  ]; then
        mkdir /usr/share/newbooks
        cp -r bin /usr/share/newbooks
fi
echo "which instance?"
koha-list --enabled
read instance
echo "slider or static?"
read slidertype
if [ "$slidertype" = "static" ]; then
    echo "search how many?"
    read numtosearch
    echo "show how many?"
    read numtoshow
    echo "target _new or self?"
    read target
    echo "Use acquisitions instead of accessiondate? (1=yes, 0=no)"
    read useacq
    echo "include new journals? 1=yes, 0=no"
    read journals
    if [ $journals = 1 ]; then
       echo "display how many journals? (10 is default)"
       read journalstoshow
    fi 
    echo "#!/bin/sh

env PERL5LIB=/usr/share/koha/lib KOHA_CONF=/etc/koha/sites/$instance/koha-conf.xml /usr/share/newbooks/bin/koha-recent.pl --config /etc/koha/sites/$instance/$instance-koha-recent.config --numtosearch $numtosearch --numtoshow $numtoshow --numjournalstoshow $journalstoshow --layout table --target $target --useacq $useacq > /tmp/$instance-books
cp /tmp/$instance-books /var/www/files/$instance/books.html" > /usr/local/sbin/$instance-newbooks.sh
    echo "---
kohaOpacUrl: /cgi-bin/koha
newJournals: $journals" > /etc/koha/sites/$instance/$instance-koha-recent.config
else
    echo "how many covers across per page?"
    read numtoshow
    echo "how many high per page?"
    read numhigh
    echo "how many pages to cycle through?"
    read numpages
    echo "Use google for covers? (1=yes, 0=no, default is Amazon.)"
    read usegoogle
    echo "---
kohaOpacUrl: /cgi-bin/koha
newJournals: 0
width: $numtoshow
height: $numhigh
pages: $numpages
template: koha-recent-slider.tt" > /usr/share/koha/bin/$instance-koha-recent-slider.config
    if [ $usegoogle = 1 ]; then
       echo "source: google" >> /usr/share/koha/bin/$instance-koha-recent-slider.config
    fi
    echo "#!/bin/sh
cd /usr/share/koha/bin/

env KOHA_CONF=/etc/koha/sites/$instance/koha-conf.xml PERL5LIB=/usr/share/koha/lib perl /usr/share/koha/bin/koha-recent-slider.pl --config /usr/share/koha/bin/$instance-koha-recent-slider.config | tee /var/www/files/$instance/recentbooks.html" > /usr/local/sbin/$instance-newbooks.sh

    sudo cp bin/koha-recent-slider.pl /usr/share/koha/bin/ 
    sudo cp -r bin/AnythingSlider /usr/share/koha/opac/htdocs/opac-tmpl/bootstrap/js/
fi

chmod +x /usr/local/sbin/$instance-newbooks.sh

echo "# update the new books for the $instance install

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
20 6 * * * $instance-koha /usr/local/sbin/$instance-newbooks.sh" > /etc/cron.d/$instance-newbooks
echo "cronjob installed"
echo "Running this for the first time..."
sudo -u $instance-koha /usr/local/sbin/$instance-newbooks.sh


