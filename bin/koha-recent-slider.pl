#!/usr/bin/perl

use strict;
use warnings;
use C4::Context;
use CGI;
use Business::ISBN;
use YAML;
use JSON;
use LWP::UserAgent;
use HTTP::Request;
use Getopt::Long;
use File::Slurp;
use Data::Dumper;

use Template;

my ( $config_file ) =
  ( undef);

GetOptions(
    'config=s'      => \$config_file,
);

die "You need to specify a config file with --config.\n" if !$config_file;

my $config = read_file($config_file);
my $conf   = Load $config;

# Verify that things we need are in the config file
die "The config file must contain \"width\" which is the number of entries to show across the page\n" if !$conf->{width};
die "The config file must contain \"height\" which is the number of entries to show down the page\n" if !$conf->{height};
die "The config file must contain \"template\" which is the full path to the template used to generate the HTML.\n" if !$conf->{template};
die "The config file must contain \"pages\" which is the the number of pages that will be displayed.\n" if !$conf->{pages};

my $dbh = C4::Context->dbh();

my $sqlQuery = <<EOH;
SELECT biblio.biblionumber AS bnum, biblio.title,biblio.author, biblioitems.isbn
FROM biblio,items,biblioitems
WHERE items.biblionumber = biblio.biblionumber AND biblioitems.biblionumber = biblio.biblionumber
    AND isbn IS NOT NULL
GROUP by biblio.biblionumber
ORDER by dateaccessioned DESC
LIMIT 2000
EOH

my $uni = $dbh->prepare("set names utf8;");
my $sth = $dbh->prepare($sqlQuery);
$uni->execute();
$sth->execute();

my $query;

my @books;
while ( ( my $ref = $sth->fetchrow_hashref() ) && @books <= $conf->{height} * $conf->{width} * $conf->{pages}) {
    if ( defined( $ref->{'isbn'} ) ) {
        $ref->{'isbn'} =~ s/\|.*//;
        my $isbn = Business::ISBN->new( $ref->{'isbn'} );
        next if !$isbn;
        $isbn = $isbn->as_isbn10;
        next if !$isbn;
        my $isbn10 = $isbn->isbn;
        my $img;
# google or amazon?
        if (defined ($conf->{source}) && $conf->{source} eq "google" ) { #if we asked for google
             my $URL = "https://books.google.com/books?bibkeys=$isbn10&jscmd=viewapi"; #the url for the json
             my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });  #has to be https
             $ua->agent('Mozilla/5.0'); #google gets cross if you use lwp/perl6 as the useragent.
             my $header = HTTP::Request->new(GET => $URL);  
             my $request = HTTP::Request->new('GET', $URL, $header);  
             my $response = $ua->request($request);  
             if ($response->is_success){
                my $content = $response->{_content};
                $content =~ s/^.*:\{/\{/;
                $content =~ s/..$//;
                my $json = decode_json($content);
                $img = $json->{'thumbnail_url'};
#warn Dumper($json);
             }elsif ($response->is_error){  
                print "Error: not found in google\n";  
             }  
       }else { #use amazon
            $img =
             'http://images.amazon.com/images/P/'
              . $isbn10
              . '.01._THUMBZZZ_PB_PU0_.jpg';
       }
       next if !hasImage($img);

       push @books, {
           url => $conf->{kohaOpacUrl} . '/opac-detail.pl?biblionumber=' . $ref->{bnum},
           title => $ref->{title},
           author => $ref->{author},
           img => $img,
       }
   }
}

$sth->finish();

# Find all the serials that have been received (status=2)
my $serials_query=<<EOQ;
SELECT * FROM
    (SELECT biblio.biblionumber, title, serialseq,publisheddate, status
    FROM serial,biblio
    WHERE biblio.biblionumber=serial.biblionumber
        AND status=2
        AND publisheddate IS NOT NULL
    ORDER BY publisheddate DESC) AS serials
GROUP BY biblionumber
ORDER BY publisheddate DESC
LIMIT 10
EOQ
my @journals;
if ( $conf->{newJournals} ) {
    $sth = $dbh->prepare($serials_query);
    $sth->execute();
    while ( my $row = $sth->fetchrow_hashref() ) {
        push @journals, {
            url => $conf->{kohaOpacUrl} . 'opac-detail.pl?biblionumber=' . $row->{biblionumber},
            title => $row->{title},
            serialseq => $row->{serialseq},
        }
    }
}

# Disconnect from the database.
$dbh->disconnect();

my $template_data = {
    books => \@books,
    journals => \@journals,
    height => $conf->{height},
    width => $conf->{width},
    show_books => 1,
    show_journals => $conf->{newJournals},
};
my $tt = Template->new() || die "Unable to instantiate Template: ".Template->error()."\n";
$tt->process($conf->{template}, $template_data) || die "Unable to process the template:". $tt->error()."\n";

sub hasImage {
    my ($URL_in) = @_;
    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);
    my $resp = $ua->get($URL_in);
    my $size = $resp->header('Content-Length');
# Amazon pretty much always gives us a successful response, but if the size is small, we should ignore it.
    if ( defined $size && $resp && ($size > 1000) && ($resp->content_type eq "image/jpeg" )) {
        return 1;
    }
    else {
        return 0;
    }
}
